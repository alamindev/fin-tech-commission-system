## Fintech Commission system

Pre-requirements:
- PHP >=7.3 
- Composer

Following steps:
 - Composer install
 - run php -S localhost:8088 /project_directory/index.php
 - Open in any browser http://localhost:8088
 - Here used input.csv (in project root directory) as example, 
   just change data in csv or change csv file path in index.php
 
 Thanks :)
