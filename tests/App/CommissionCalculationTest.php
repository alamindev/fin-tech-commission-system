<?php

declare(strict_types=1);

namespace Fintech\Commission\Tests\App;

use Fintech\Commission\App\CommissionCalculation;
use Fintech\Commission\Service\ClientType;
use Fintech\Commission\Service\WeeklyLimit;
use PHPUnit\Framework\TestCase;

class CommissionCalculationTest extends TestCase
{
    /**
     */
    private $transactions;

    public function setUp()
    {
        $this->transactions = new CommissionCalculation('input.csv');
    }

    /**
     *  Check transaction test
     * @param array $input
     * @param float $output
     * @dataProvider dataProviderForCommissionTesting
     */
    public function testCommission(array $input, float $output)
    {
        $this->assertEquals(
            $output,
            $this->transactions->calculation($input),
        );
    }

    public function dataProviderForCommissionTesting(): array
    {
        return [
            '1st low amount from total free amount. so, no commission' => [['2021-12-31', '1', 'private', 'withdraw', '1000.00', 'EUR'], 0.00],
            '1st transaction full free amount was deducted, total commission' => [['2021-12-31', 1, 'private', 'withdraw', 1000.00, 'EUR'], 3.00],
        ];
    }
}
