<?php

declare(strict_types=1);

namespace Fintech\Commission\Tests\Service;

use Fintech\Commission\Service\CsvReader;
use PHPUnit\Framework\TestCase;

class CsvReaderTest extends TestCase
{
    /**
     * @var CsvReader
     */
    private $file;

    public function setUp()
    {
        $this->file = new CsvReader('input.csv');
    }

    /**
     * @param string $key
     * @dataProvider dataProviderForAddTesting
     */
    public function testReadCorrectDataFromCSV(string $key)
    {
        $this->assertArrayHasKey(
            $key,
            $this->file->rows()->current()
        );
    }

    public function dataProviderForAddTesting(): array
    {
        return [
            'csv file first row exist 5 index' => [5],
        ];
    }
}
