<?php

declare(strict_types=1);

namespace Fintech\Commission\Tests\Service;

use Fintech\Commission\Service\ClientType;
use Fintech\Commission\Service\CsvReader;
use Fintech\Commission\Service\WeeklyLimit;
use PHPUnit\Framework\TestCase;

class WeeklyLimitTest extends TestCase
{
    /**
     */
    private $transaction;

    public function setUp()
    {
        //Same day multiple transaction
        $this->transaction = WeeklyLimit::getInstance(1, '2016-01-05');
    }

    /**
     *  Check exclude free commission return amount value within the week
     * @param float $originalPrice
     * @param float $returnAmount
     * @dataProvider dataProviderForDeductFreeAmountTesting
     */
    public function testDeductFreeTransaction(float $returnAmount, float $originalPrice)
    {
        $this->assertEquals(
            $returnAmount,
            $this->transaction->getPriceExceptFree($originalPrice, ClientType::getConfig('private', 'withdraw'))
        );
    }

    public function dataProviderForDeductFreeAmountTesting(): array
    {
        //current config max free commission amount 1000
        return [
            '1st low amount from total free amount. so, no commission' => [0, 500],
            '1st deduct 500 and second deduct 500 so return 200' => [200, 700],
            'maximum deduct so return all amount' => [500, 500],
        ];
    }
}
