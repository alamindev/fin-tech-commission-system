<?php

declare(strict_types=1);

namespace Fintech\Commission\Tests\Service;

use Fintech\Commission\Service\ClientType;
use Fintech\Commission\Service\CsvReader;
use PHPUnit\Framework\TestCase;

class ClientConfigTest extends TestCase
{
    /**
     *Check config value is correct
     */
    public function testConfigByClintTypeAndTransaction()
    {
        $this->assertEquals(
            ClientType::getConfig('private', 'withdraw'),
            [
                'commission' => 0.3,
                'number_of_free_transaction' => 3,
                'max_free_commission' => 1000
            ]
        );
    }
}
