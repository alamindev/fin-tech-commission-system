<?php

use Fintech\Commission\App\CommissionCalculation;

require 'vendor/autoload.php';

try {
    $transaction = new CommissionCalculation('input.csv');
    $transaction->commission();
} catch (Throwable $e) {
    echo $e->getMessage();
}