<?php

declare(strict_types=1);

namespace Fintech\Commission\App;

use Fintech\Commission\Service\ClientType;
use Fintech\Commission\Service\CsvReader;
use Fintech\Commission\Service\CurrencyExchange;
use Fintech\Commission\Service\WeeklyLimit;

class CommissionCalculation
{
    private $file;

    public function __construct(string $csvFilePath)
    {
        $this->file = new CsvReader($csvFilePath);
    }

    /**
     * Calculate the commission
     * @throws \Exception
     */
    public function commission()
    {
        foreach ($this->file->rows() as $key => $item) {
            if (empty($item[5])) {
                throw new \Exception('Data format is not correct, check row number:' . ++$key);
            }
            //print the result
            echo $this->calculation($item) . '</br>';
        }
    }

    private function getCommissionInPercentage(float $basePrice, float $commission)
    {
        return $basePrice * ($commission / 100);
    }

    /**
     * Proceed the calculation
     * @param array $item
     * @return string
     * @throws \Exception
     */
    public function calculation(array $item): string
    {
        list($date, $userId, $clintType, $transactionType, $price, $currency) = $item;
        $currencyRate = CurrencyExchange::rate($currency);
        $basePrice = $price * $currencyRate;

        $clintConfig = ClientType::getConfig($clintType, $transactionType);

        //Check Monday to Sunday transaction
        $weeklyCalculation = WeeklyLimit::getInstance((int)$userId, $date);
        $basePrice = $weeklyCalculation->getPriceExceptFree($basePrice, $clintConfig);

        //Commission amount
        $commissionAmount = $this->getCommissionInPercentage($basePrice, $clintConfig['commission']);

        //commission amount back to original currency
        $commissionAmount = $commissionAmount * (1 / $currencyRate);

        //Format amount like 0.023 to 0.03
        $precision = pow(10, 2);
        $commissionAmount = ceil($commissionAmount * $precision) / $precision;

        // 0-> 0.00
        return number_format($commissionAmount, 2, '.', '');
    }
}


