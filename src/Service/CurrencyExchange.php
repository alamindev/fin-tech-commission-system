<?php

declare(strict_types=1);

namespace Fintech\Commission\Service;

class CurrencyExchange
{
    private static $rates = [];

    /**
     * Base currency name
     * @var string
     */
    private static $baseCurrency = '';

    /**
     * Get the currency rate based on base currency
     * @param string $currency
     * @return float
     */
    public static function rate(string $currency): float
    {
        self::getExchangeRate();
        $baseCurrency = self::getBaseCurrencyRate();
        return $baseCurrency / (self::$rates[$currency] ?? $baseCurrency);
    }

    /**
     * Base currency
     * @return float
     */
    private static function getBaseCurrencyRate(): float
    {
        return self::$rates[self::$baseCurrency] ?? 1;
    }

    /**
     * Get exchange rate from exchangeratesapi.io
     * More details https://exchangeratesapi.io/documentation/
     * @return array
     */
    private static function getExchangeRate(): array
    {
        if (!self::$rates) {
            //get data from exchange rate site
            $secret = 'c258f049e2f78cea8c1cbdc32979e363';
            $curl = curl_init('http://api.exchangeratesapi.io/latest?access_key=' . $secret);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            $response = curl_exec($curl);
            curl_close($curl);
            if (!empty($response['rates'])) {
                $response = json_decode($response, true);
                self::$baseCurrency = $response['base'] ?? 'EUR';
                self::$rates = $response['rates'] ?? [];
                return self::$rates;
            }
            //set default value if anyway  no response from exchange rate
            self::$rates = [
                'USD' => 1.1497,
                'JPY' => 129.53,
                'EUR' => 1,
            ];
        }
        return self::$rates;
    }
}