<?php

declare(strict_types=1);

namespace Fintech\Commission\Service;


class ClientType
{
    /**
     * Based on clint type and transaction initial config
     * @var \array[][]
     */
    private static $clientTypes = [
        'private' => [
            'withdraw' => [
                'commission' => 0.3,
                'number_of_free_transaction' => 3,
                'max_free_commission' => 1000
            ],
            'deposit' => [
                'commission' => 0.03,
                'number_of_free_transaction' => null,
                'max_free_commission' => null
            ]
        ],
        'business' => [
            'withdraw' => [
                'commission' => 0.5,
                'number_of_free_transaction' => 0,
                'max_free_commission' => null
            ],
            'deposit' => [
                'commission' => 0.03,
                'number_of_free_transaction' => null,
                'max_free_commission' => null
            ]
        ]
    ];

    /**
     * Get client config
     * @param string $clintType
     * @param string $transactionType
     * @return array|mixed
     */
    public static function getConfig(string $clintType, string $transactionType): array
    {
        return self::$clientTypes[$clintType][$transactionType] ?? [];
    }
}