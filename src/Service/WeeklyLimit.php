<?php

declare(strict_types=1);

namespace Fintech\Commission\Service;

use DateTime;

class WeeklyLimit
{
    /**
     * @var array
     */
    private static $instances = [];
    /**
     * @var float
     */
    protected $remainingFreeCommission;
    /**
     * @var int
     */
    protected $count = 1;

    /**
     * Multiton instance based on user and transaction date
     * @param int $userId
     * @param string $date
     * @return mixed
     * @throws \Exception
     */
    public static function getInstance(int $userId, string $date): WeeklyLimit
    {
        try {
            $currentDate = new DateTime($date);
            $yearWeek = $currentDate->modify('this week')->format('YW');
            if (!isset(self::$instances[$userId][$yearWeek])) {
                self::$instances[$userId][$yearWeek] = new self();
            }
            return self::$instances[$userId][$yearWeek];
        } catch (\Exception $e) {
            throw new \Exception('Date format is not valid');
        }
    }

    /**
     * Based on user and week count transaction and mas free limit check
     * @param float $price
     * @param array $clintConfig
     * @return int|float
     */
    public function getPriceExceptFree(float $price, array $clintConfig): float
    {
        $this->remainingFreeCommission = $this->remainingFreeCommission ?? $clintConfig['max_free_commission'];

        if ($this->count <= $clintConfig['number_of_free_transaction']
            && $clintConfig['max_free_commission']
            && $this->remainingFreeCommission > 0
        ) {
            $this->count++;
            if ($this->remainingFreeCommission > $price) {
                $this->remainingFreeCommission = $this->remainingFreeCommission - $price;
                return 0;
            }
            $price = $price - $this->remainingFreeCommission;
            $this->remainingFreeCommission = 0;
        }
        return $price;
    }
}