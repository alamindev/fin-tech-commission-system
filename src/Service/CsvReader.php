<?php

declare(strict_types=1);

namespace Fintech\Commission\Service;

class CsvReader
{
    protected $file;

    /**
     * Get csv file path
     * CsvReader constructor.
     * @param $filePath
     * @throws \Exception
     */
    public function __construct(string $filePath)
    {
        if (!file_exists($filePath)) {
            throw new \Exception('Csv filepath is not correct');
        }

        $this->file = fopen($filePath, 'r');
    }

    public function rows()
    {
        while (!feof($this->file)) {
            yield fgetcsv($this->file, 4096);
        }
        return;
    }
}